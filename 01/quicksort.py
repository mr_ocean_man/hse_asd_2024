"""
module with quicksort
"""

import random
from typing import List


class QuickSort:
    """
    class with realization quicksort algorithm
    """

    def quicksort(self, nums: List[tuple]) -> List[tuple]:
        """

        :param nums: list containing tuples for sorting
        :return: sorted list containing tuples
        """
        if len(nums) <= 1:
            self.sorted_values = nums
            return self.sorted_values

        random_q = random.choice(nums)
        s_nums = []
        m_nums = []
        e_nums = []
        for num in nums:
            if num < random_q:
                s_nums.append(num)
            elif num > random_q:
                m_nums.append(num)
            else:
                e_nums.append(num)
        sorted_values = self.quicksort(s_nums) + e_nums + self.quicksort(m_nums)
        return sorted_values
