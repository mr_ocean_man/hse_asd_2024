"""
Module with test for quicksort algorithm
"""

from typing import List
import unittest
from quicksort import QuickSort


def input_from_file(input_path) -> List[tuple]:
    """

    :param: input file path
    :return: list containing tuples from input file
    """
    input_values = []
    with open(input_path, 'r', encoding='utf-8') as file:
        for line in file.readlines():
            line = line.split('\t')
            line[1] = line[1].replace('\n', '')
            input_values.append((int(line[0]), line[1]))

        file.close()
    return input_values


def write_to_file(sorted_values, output_path):
    """

    :param sorted_values: sorted array
    :param output_path: output file path
    :return:
    """
    with open(output_path, 'w', encoding='utf-8') as file:
        for item in sorted_values:
            output_string = f'{item[0]}\t{item[1]}\n'
            file.write(output_string)

        file.close()


class TestQuickSort(unittest.TestCase):
    """
    Test class for quicksort algorithm
    """

    def setUp(self) -> None:
        """
        :return:
        """
        self.input_path = 'hw1_files/input_hw1.txt'
        self.output_path = 'hw1_files/output_hw1.txt'
        self.test_values = [(7, 'kbGoAaUhkK'),
                            (46, 'BOhMMCiuwz'),
                            (47, 'LiFMOIUcap'),
                            (49, 'GJKyXNJuPN'),
                            (19, 'fTrZaHKFkk'),
                            (31, 'dgiFATToGK'),
                            (21, 'JdSdjJlULL'),
                            (32, 'sOWjfXtcCt'),
                            (27, 'VPtRFmWgCI'),
                            (6, 'vXANxciIts'),
                            (21, 'VebHNUjWyG')]
        self.sort = QuickSort()

    def test_input(self):
        """
        Test input_from_file function
        :return:
        """
        input_values = input_from_file(self.input_path)

        assert input_values == self.test_values

    def test_output(self):
        """
        Test write_to_file function
        :return:
        """
        write_to_file(self.test_values, self.output_path)
        input_values_test = input_from_file(self.output_path)

        assert self.test_values == input_values_test

    def test_quick_sort_with_duplicate_els(self):
        """
        Test quick sort for list containing duplicate elements
        :return:
        """
        test_values = [(7, 'kbGoAaUhkK'),
                       (5, 'BOhMMCiuwz'),
                       (5, 'LiFMOIUcap'),
                       (5, 'GJKyXNJuPN'),
                       (5, 'fTrZaHKFkk'),
                       ]

        sorted_array = self.sort.quicksort(test_values)

        truth_values = [(5, 'BOhMMCiuwz'),
                        (5, 'GJKyXNJuPN'),
                        (5, 'LiFMOIUcap'),
                        (5, 'fTrZaHKFkk'),
                        (7, 'kbGoAaUhkK')]

        assert sorted_array == truth_values

    def test_quick_sort_example(self):
        """
        Test quicksort for data from github
        :return:
        """
        sorted_array = self.sort.quicksort(self.test_values)
        truth_values = [(6, 'vXANxciIts'),
                        (7, 'kbGoAaUhkK'),
                        (19, 'fTrZaHKFkk'),
                        (21, 'JdSdjJlULL'),
                        (21, 'VebHNUjWyG'),
                        (27, 'VPtRFmWgCI'),
                        (31, 'dgiFATToGK'),
                        (32, 'sOWjfXtcCt'),
                        (46, 'BOhMMCiuwz'),
                        (47, 'LiFMOIUcap'),
                        (49, 'GJKyXNJuPN')]

        assert sorted_array == truth_values

    def test_quick_sort_one_el(self):
        """
        Test quicksort for list containing one element
        :return:
        """
        test_values = [(7, 'kbGoAaUhkK')]
        sorted_array = self.sort.quicksort(test_values)

        assert test_values == sorted_array

    def test_quick_sort_empty_arr(self):
        """
        Test quicksort for empty list
        :return:
        """
        test_values = []
        sorted_array = self.sort.quicksort(test_values)

        assert test_values == sorted_array

    def test_quick_sort_full(self):
        """
        Test full algorithm in homework task
        :return:
        """
        test_values = input_from_file(self.input_path)
        sorted_array = self.sort.quicksort(test_values)
        write_to_file(sorted_array, self.output_path)
        sorted_array_from_file = input_from_file(self.output_path)

        assert sorted_array == sorted_array_from_file


if __name__ == '__main__':
    unittest.main()
