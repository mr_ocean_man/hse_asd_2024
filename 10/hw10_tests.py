"""
module with tests for change task
"""

import unittest
from change import exchange
from typing import List


class TestChangeAlgo(unittest.TestCase):
    """
    Class with test cases
    """


    def test_first(self):
        money = [1, 2, 5, 10]
        S = 11
        real = [10, 1]

        self.assertEqual(exchange(money, S), real)
    

    def test_second(self):
        money = [1, 2, 5, 10]
        S = 9 
        real = [5, 2, 2]
        
        self.assertEqual(exchange(money, S), real)
    
    def test_third(self):
        money = [2, 5, 10]
        S = 1
        real = []

        self.assertEqual(exchange(money, S), real)
    
    def test_fourth(self):
        money = [1, 2, 5]
        S = 11
        real = [5, 5, 1]
        
        self.assertEqual(exchange(money, S), real)

if __name__ == "__main__":
    unittest.main()