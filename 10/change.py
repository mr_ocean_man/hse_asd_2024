"""
module with change problem solution
"""

import math
from typing import List


def exchange(money: List[int], cell: int) -> List:
    """solution change problem

    Args:
        money (List[int]): list with money denomination
        cell (int): total amount

    Returns:
        List: money denomination answer
    """

    ans = []
    for coin in money[::-1]:
        coin_cnt = math.floor(cell / coin)
        ans.extend([coin] * coin_cnt)
        cell -= coin_cnt * coin

    return ans
