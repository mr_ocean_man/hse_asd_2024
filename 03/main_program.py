"""
Main program implementation
"""
import pickle
from map import Map


class MainProgram:
    """
    Main program implementation
    """

    def __init__(self):
        self._map_object = Map()

    def dump_state(self, output_state_file_path: str) -> str:
        """
        Dump self._map_object state
        :param output_state_file_path: path to binary file
        :return:
        """
        try:
            with open(output_state_file_path, "wb") as output_state_file:
                pickle.dump(self._map_object, output_state_file)

            return "OK"

        except pickle.PickleError as ex:
            print(ex)

    def load_state(self, input_state_file_path: str) -> str:
        """
        Load self._map_object state
        :param input_state_file_path:  path to binary file
        :return:
        """
        try:
            with open(input_state_file_path, "rb") as input_state_file:
                self._map_object = pickle.load(input_state_file)

            return "OK"

        except FileNotFoundError as ex:
            print(ex)

    def __call__(self, input_data_path: str) -> str:
        """
        Main program call
        :param input_data_path:
        :return:
        """
        if input_data_path.endswith(".txt"):
            with open(input_data_path, "r", encoding='utf-8') as input_file:
                output_str = ''
                for line in input_file.readlines():
                    line = line.replace("\n", "")
                    line = line.split(" ")

                    if len(line) == 1:
                        map_item = self._map_object[line[0].lower()]
                        if map_item:
                            output_str += f"OK: {map_item}\n"

                        else:
                            output_str += "NoSuchWord\n"

                    elif len(line) == 2 and line[0] == '-':
                        if self._map_object[line[1].lower()]:
                            del self._map_object[line[1].lower()]
                            output_str += "OK\n"

                        else:
                            output_str += "NoSuchWord\n"

                    if len(line) == 3:
                        if line[1] == "Save":
                            output_str += self.dump_state(line[2]) + "\n"

                        elif line[1] == "Load":
                            output_str += self.load_state(line[2]) + "\n"

                        elif line[0] == "+":
                            key = line[1].lower()
                            if not self._map_object.avlt.search(key):
                                self._map_object[key] = int(line[2])
                                output_str += "OK\n"

                            else:
                                output_str += "Exist\n"

                        else:
                            raise ValueError(
                                f"input {line[0]}, but expected '!'"
                            )

        else:
            raise FileNotFoundError("Incorrect file path")

        return output_str


if __name__ == "__main__":
    main_program = MainProgram()
    print(main_program("test_input.txt"))
