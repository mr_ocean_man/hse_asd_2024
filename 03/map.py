"""
AVL tree map implementation
"""
from typing import Any, Optional
from avl_tree import AVLTree


class Map:
    """
    Key-value map implemented on AVL tree
    """

    def __init__(self) -> None:
        self._avlt = AVLTree()

    def __setitem__(self, key: str, value: int) -> None:
        """
        Insert (key, value) in map
        :param key:
        :param value:
        :return:
        """

        self._avlt.insert(key=key, data=value)

    def __getitem__(self, key: str) -> Optional[Any]:
        """
        Get the data by given key
        :param key:
        :return:
        """
        node = self._avlt.search(key=key)
        if node:
            return node.data
        return None

    def __delitem__(self, key: str) -> None:
        """
        Remove (key, value) from map
        :param key:
        :return:
        """
        self._avlt.delete(key=key)

    @property
    def avlt(self):
        return self._avlt
