"""
Module with test for AVL tree
"""

import unittest
from main_program import MainProgram


class TestAVLTree(unittest.TestCase):
    """
    Test class for AVL tree
    """

    def setUp(self) -> None:
        """

        :return:
        """
        self.main_program = MainProgram()

    def test_add_node(self):
        """
        Test add node in AVL tree
        :return:
        """
        input_file = "hw3_files/input_test_add_node.txt"
        output_str = self.main_program(input_file)
        test_str = "OK\n"

        self.assertEqual(output_str, test_str)

    def test_add_delete_node(self):
        """
        Test add and delete node in AVL tree
        :return:
        """
        input_file = "hw3_files/input_test_delete_node.txt"
        output_str = self.main_program(input_file)
        test_str = "OK\nOK\n"
        self.assertEqual(output_str, test_str)

    def test_delete_empty_node(self):
        """
        Test delete empty node
        :return:
        """
        input_file = "hw3_files/input_test_empty_tree.txt"
        output_str = self.main_program(input_file)
        test_str = "NoSuchWord\n"

        self.assertEqual(output_str, test_str)

    def test_dump_and_load(self):
        """
        Test dump and load AVL tree map
        :return:
        """
        input_file = "hw3_files/input_test_dump_and_load.txt"
        output_str = self.main_program(input_file)
        test_str = "OK\nOK\nOK\nOK\n"

        self.assertEqual(test_str, output_str)

    def test_add_diff_register(self):
        """
        Test add node with diff register
        :return:
        """
        input_file = "hw3_files/input_test_add_registr.txt"
        output_str = self.main_program(input_file)
        test_str = "OK\nExist\n"

        self.assertEqual(test_str, output_str)

    def test_full_program(self):
        """
        Test all program modules
        :return:
        """
        input_file = "hw3_files/input_test_full_circle.txt"
        output_str = self.main_program(input_file)
        test_str = "OK\nExist\nOK\nOK: 18446744073709551615\nOK: 1\nOK\nNoSuchWord\nOK\nOK\n"

        self.assertEqual(output_str, test_str)

    def test_dump_error(self):
        """
        Test raise in dump method
        :return:
        """
        input_file = "hw3_files/input_test_dump_error.txt"
        with self.assertRaises(Exception) as context:
            self.main_program(input_file)

        self.assertTrue('No such file or directory' in str(context.exception))

    def test_add_similar_sym(self):
        """
        Test add similar symbols
        :return:
        """
        input_file = "hw3_files/input_test_add_similar_sym.txt"
        output_str = self.main_program(input_file)
        test_str = "OK\nExist\n"

        self.assertEqual(output_str, test_str)


if __name__ == "__main__":
    unittest.main()
