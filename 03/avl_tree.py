"""
avl_tree realization
"""
from dataclasses import dataclass
from typing import Optional


@dataclass
class Node:
    """
    Node class for AVL tree
    """
    key: str
    data: int
    left: Optional["Node"] = None
    right: Optional["Node"] = None
    parent: Optional["Node"] = None
    height: int = 0


class AVLTree:
    """
    AVLTree implementation
    """

    def __init__(self) -> None:
        self.root: Optional[Node] = None

    def search(self, key: str) -> Optional[Node]:
        """
        Search item in AVL tree
        :param key: key in AVL tree item
        :return: node if key in AVL tree else None
        """
        current = self.root

        while current:
            if key < current.key:
                current = current.left
            elif key > current.key:
                current = current.right
            else:
                return current
        return None

    def insert(self, key: str, data: int) -> None:
        """
        Insert item in AVL tree
        :param key: key for insert
        :param data: data for insert in node
        :return:
        """
        new_node = Node(key=key, data=data)
        parent: Optional[Node] = None
        current: Optional[Node] = self.root
        while current:
            parent = current
            if new_node.key < current.key:
                current = current.left
            elif new_node.key > current.key:
                current = current.right

        new_node.parent = parent
        if parent is None:
            self.root = new_node
        else:
            if new_node.key < parent.key:
                parent.left = new_node
            else:
                parent.right = new_node

            if not (parent.left and parent.right):
                self._insert_fixup(new_node)

    def delete(self, key: str) -> None:
        """
        Delete item from AVL tree
        :param key: node key for delete
        :return:
        """
        if self.root and (deleting_node := self.search(key=key)):
            if (deleting_node.left is None) and (deleting_node.right is None):
                self._delete_no_child(deleting_node=deleting_node)
            elif deleting_node.left and deleting_node.right:
                replacing_node = self.get_leftmost(node=deleting_node.right)
                deleting_node.key = replacing_node.key
                deleting_node.data = replacing_node.data
                if replacing_node.right:
                    self._delete_one_child(deleting_node=replacing_node)
                else:
                    self._delete_no_child(deleting_node=replacing_node)
            else:
                self._delete_one_child(deleting_node=deleting_node)

    @staticmethod
    def get_height(node: Optional[Node]) -> int:
        """

        :param node:
        :return: node height
        """
        if node:
            return node.height

        return -1

    @staticmethod
    def get_leftmost(node: Node) -> Node:
        """

        :param node:
        :return: left children in node
        """
        current_node = node
        while current_node.left:
            current_node = current_node.left
        return current_node

    @staticmethod
    def get_rightmost(node: Node) -> Node:
        """

        :param node:
        :return: right children in node
        """
        current_node = node
        while current_node.right:
            current_node = current_node.right
        return current_node

    def fix_height(self, node_x: Node) -> None:
        """
        Fix node height after balancing
        :param node_x: node for fix
        :return:
        """
        height_left = self.get_height(node_x.left)
        height_right = self.get_height(node_x.right)
        if height_left > height_right:
            node_x.height = height_left + 1
        else:
            node_x.height = height_right + 1

    def _get_balance_factor(self, node: Optional[Node]) -> int:
        """

        :param node:
        :return: balance factor for node
        """
        if node:
            return self.get_height(node.right) - self.get_height(node.left)

        return -1

    def _left_rotate(self, node_x: Node) -> None:
        """
        Left rotate for AVL tree
        :param node_x: current node
        :return:
        """
        node_y = node_x.right
        if node_y:
            node_x.right = node_y.left
            if node_y.left:
                node_y.left.parent = node_x
            node_y.parent = node_x.parent
            if node_x.parent is None:
                self.root = node_y
            elif node_x == node_x.parent.left:
                node_x.parent.left = node_y
            else:
                node_x.parent.right = node_y

            node_y.left = node_x
            node_x.parent = node_y

            node_x.height = 1 + max(
                self.get_height(node_x.left), self.get_height(node_x.right)
            )
            node_y.height = 1 + max(
                self.get_height(node_y.left), self.get_height(node_y.right)
            )

    def _right_rotate(self, node_x: Node) -> None:
        """
        Right rotate for AVL tree
        :param node_x: current node
        :return:
        """
        node_y = node_x.left
        if node_y:
            node_x.left = node_y.right
            if node_y.right:
                node_y.right.parent = node_x
            node_y.parent = node_x.parent
            if node_x.parent is None:
                self.root = node_y
            elif node_x == node_x.parent.right:
                node_x.parent.right = node_y
            else:
                node_x.parent.left = node_y

            node_y.right = node_x
            node_x.parent = node_y

            node_x.height = 1 + max(
                self.get_height(node_x.left), self.get_height(node_x.right)
            )
            node_y.height = 1 + max(
                self.get_height(node_y.left), self.get_height(node_y.right)
            )

    def _insert_fixup(self, new_node: Node) -> None:
        """
        Balancing AVL tree after insert
        :param new_node: inserted node
        :return:
        """
        parent = new_node.parent

        while parent:
            parent.height = 1 + max(
                self.get_height(parent.left), self.get_height(parent.right)
            )
            grandparent = parent.parent
            if grandparent:
                if self._get_balance_factor(grandparent) > 1:
                    if self._get_balance_factor(parent) >= 0:
                        self._right_rotate(grandparent)
                    elif self._get_balance_factor(parent) < 0:
                        self._left_rotate(parent)
                        self._right_rotate(grandparent)
                elif self._get_balance_factor(grandparent) < -1:
                    if self._get_balance_factor(parent) <= 0:
                        self._left_rotate(grandparent)
                    elif self._get_balance_factor(parent) > 0:
                        self._right_rotate(parent)
                        self._left_rotate(grandparent)

                break
            parent = parent.parent

    def _transplant(self, deleting_node: Node,
                    replacing_node: Optional[Node]) -> None:
        """
        Transplant node in AVL tree be unbalanced after operation delete
        :param deleting_node:
        :param replacing_node:
        :return:
        """
        if deleting_node.parent is None:
            self.root = replacing_node
        elif deleting_node == deleting_node.parent.left:
            deleting_node.parent.left = replacing_node
        else:
            deleting_node.parent.right = replacing_node

        if replacing_node:
            replacing_node.parent = deleting_node.parent

    def _delete_no_child(self, deleting_node: Node) -> None:
        """
        Method for the case that the node to be deleted has no child
        :param deleting_node:
        :return:
        """
        parent = deleting_node.parent
        self._transplant(deleting_node=deleting_node, replacing_node=None)
        if parent:
            self._delete_fixup(fixing_node=parent)

    def _delete_one_child(self, deleting_node: Node) -> None:
        """
        Method for the case that the node to be deleted has one child
        :param deleting_node:
        :return:
        """
        parent = deleting_node.parent
        replacing_node = (
            deleting_node.right if deleting_node.right else deleting_node.left
        )
        self._transplant(deleting_node=deleting_node,
                         replacing_node=replacing_node)
        if parent:
            self._delete_fixup(fixing_node=parent)

    def _delete_fixup(self, fixing_node: Node) -> None:
        """
        Method for balancing tree after deleting node
        :param fixing_node:
        :return:
        """
        while fixing_node:
            fixing_node.height = 1 + max(
                self.get_height(fixing_node.left),
                self.get_height(fixing_node.right)
            )

            if self._get_balance_factor(fixing_node) > 1:
                if self._get_balance_factor(fixing_node.left) >= 0:
                    self._right_rotate(fixing_node)
                elif self._get_balance_factor(fixing_node.left) < 0:
                    self._left_rotate(fixing_node.left)
                    self._right_rotate(fixing_node)
            elif self._get_balance_factor(fixing_node) < -1:
                if self._get_balance_factor(fixing_node.right) <= 0:
                    self._left_rotate(fixing_node)
                elif self._get_balance_factor(fixing_node.right) > 0:
                    self._right_rotate(fixing_node.right)
                    self._left_rotate(fixing_node)

            fixing_node = fixing_node.parent
