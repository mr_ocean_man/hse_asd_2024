"""
module with tests for rectangle task
"""

import unittest
from rectangle import maximal_rectangle
from typing import List


class TestRectangleAlgo(unittest.TestCase):
    """
    Class with test cases
    """


    def test_first(self):
        matrix = [
            [0, 1, 1, 0],
            [1, 1, 1, 1],
            [1, 1, 1, 1],
            [1, 1, 0, 0]
            ]
        height = 4
        width = 4

        self.assertEqual(maximal_rectangle(matrix, height, width), 2)

    

    def test_second(self):
        height = 4
        width = 5
        matrix = [
            [0, 1, 0, 1, 1],
            [1, 0, 0, 0, 1],
            [0, 1, 0, 0, 0],
            [1, 1, 0, 1, 1]
            ]

        self.assertEqual(maximal_rectangle(matrix, height, width), 4)
    
    def test_third(self):
        matrix = [
            [0, 1, 1, 0],
            [1, 1, 1, 1],
            [1, 1, 1, 1],
            [1, 1, 1, 0]
            ]
        height = 4
        width = 4

        self.assertEqual(maximal_rectangle(matrix, height, width), 1)
    
    def test_fourth(self):
        matrix = [
            [1, 1, 1, 1],
            [1, 1, 1, 1],
            [1, 1, 1, 1],
            [1, 1, 1, 1]
            ]
        height = 4
        width = 4

        self.assertEqual(maximal_rectangle(matrix, height, width), 0)

if __name__ == "__main__":
    unittest.main()