"""
module with change problem solution
"""
from typing import List


def maximal_rectangle(matrix: List[List[int]], height: int, width: int) -> int:
    """solution tash with search max rectangle

    Args:
        matrix (List[List[int]]): input matrix with 0 and 1

    Returns:
        int: maximum rectangle area from 0
    """
    if not matrix:
        return 0

    left = [0] * width
    right = [width] * width
    height_arr = [0] * width

    max_area = 0

    for i in range(height):
        cur_left, cur_right = 0, width

        for j in range(width):
            if matrix[i][j] == 0:
                height_arr[j] += 1
            else:
                height_arr[j] = 0

        for j in range(width):
            if matrix[i][j] == 0:
                left[j] = max(left[j], cur_left)
            else:
                left[j] = 0
                cur_left = j + 1

        for j in range(width-1, -1, -1):
            if matrix[i][j] == 0:
                right[j] = min(right[j], cur_right)
            else:
                right[j] = width
                cur_right = j

        for j in range(width):
            max_area = max(max_area, (right[j] - left[j]) * height_arr[j])

    return max_area
