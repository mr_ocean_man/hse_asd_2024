from hashtable import HashTable, INITIAL_BUCKET_COUNT
import unittest

class TestHashMap(unittest.TestCase):

    def test_get_returns_value_for_associated_key(self):
        m = HashTable()
        for n in range(0, 8):
            m[n] = f'val{n}'
        for n in range(0, 8):
            self.assertEqual(m[n], f'val{n}')

    def test_get_returns_value_for_supplied_key_after_resize(self):
        m = HashTable()
        for n in range(0, 40):
            m[f'key{n}'] =  f'val{n}'
        for n in range(0, 40):
            self.assertEqual(m[f'key{n}'], f'val{n}')

    def test_len_increases_by_one_on_put_and_decreases_by_one_on_remove(self):
        m = HashTable()
        for n in range(0, 20):
            m[n] = n
            self.assertEqual(len(m), n + 1)
        for n in range(0, 20):
            m.remove(n)
            self.assertEqual(len(m), 19 - n)

    def test_remove_removes_entry_for_supplied_key_and_returns_associated_value(self):
        m = HashTable()
        m[1] = 2
        self.assertEqual(len(m), 1)
        self.assertEqual(m.remove(1), 2)
        self.assertTrue(m[1] is None)
        self.assertEqual(len(m), 0)

    def test_value_match_supplied_values(self):
        m = HashTable()
        m[1] = 2
        self.assertTrue(m.remove(1, 3, True) is None)
        self.assertEqual(len(m), 1)
        self.assertEqual(m.remove(1, 2), 2)
        self.assertEqual(len(m), 0)

    def test_get_size(self):
        m = HashTable()
        m[1] = 2
        m[2] = 3
        self.assertEqual(m.get_size(), 2)
    
    def test_get_capacity(self):
        m = HashTable()
        self.assertEqual(m.get_capacity(), INITIAL_BUCKET_COUNT)
    
    def test_clear(self):
        m = HashTable()
        m[1] = 2
        m[2] = 3
        m.clear()
        self.assertEqual(m.get_size(), 0)
    
if __name__ == "__main__":
    unittest.main()