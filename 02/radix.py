from typing import Sequence


def sort(arr: Sequence[int], key=None):
    if len(arr) < 2:
        return arr

    if key is None:
        key = lambda x: x

    keys = tuple(key(a) for a in arr)
    lengths = tuple(len(k) for k in keys)

    for digit in range(max(lengths)):
        sorted_keys = count_sort(
            [
                (k[-digit - 1] if len(k) > digit else 0, k, a)
                for k, a in zip(keys, arr)
            ],
            key=lambda x: x[0],
        )

        arr = [a for _, _, a in sorted_keys]
        keys = [k for _, k, _ in sorted_keys]

    return arr


def count_sort(arr: Sequence[int], key):
    max_key = max(key(p) for p in arr)
    min_key = min(key(p) for p in arr)

    counts = [0 for _ in range(min_key, max_key + 1)]
    arr_sorted = [None for _ in arr]

    key_ = lambda x: key(x) - min_key

    for p in arr:
        counts[key_(p)] += 1

    for i, c in enumerate(counts[1:], start=1):
        counts[i] = counts[i - 1] + c

    for p in arr[::-1]:
        c_idx = key_(p)
        idx = counts[c_idx] - 1
        counts[c_idx] -= 1
        arr_sorted[idx] = p

    return arr_sorted
