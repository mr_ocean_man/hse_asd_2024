# pylint: disable=unnecessary-lambda-assignment, missing-module-docstring

# to avoid messing with python imports,
# put tests into the same package
import random
from dataclasses import dataclass

import unittest
from radix import count_sort, sort


def phone_numbers():
    return (
        ("921", "1102", "48", "5", "44", "700"),
        (
            "100-10",
            "150-10",
            "10-50",
            "77-77",
        ),
        (),
        (
            "+375-123-1234567",
            "+7-495-1123212",
            "+375-123-1234567",
            "+7-495-1123212",
            "+7-499-1123212",
            "+7-495-1154212",
        ),
    )

def random_byte_strings():
    random.seed(0)

    num_items = 100
    num_digits = 20
    num_cases = 10

    def generate(f):
        return tuple([f() for _ in range(num_items)] for _ in range(num_cases))

    return generate(lambda: tuple(random.randbytes(num_digits)))


def random_numbers():
    random.seed(0)
    return tuple(random.randint(-100, 100) for _ in range(2000))

class TestRadix(unittest.TestCase):

    def test_phone_numbers(self):
        for numbers in phone_numbers():
            numbers = [tuple(map(int, x.strip("+").split("-"))) for x in numbers]

            sortable = list(numbers)

            referemce_array = list(numbers)
            referemce_array.sort()

            self.assertEqual(sort(sortable), referemce_array)

    def test_random_byte_string_sort(self):
        for items in random_byte_strings():
            reference_array = list(items)
            reference_array.sort()

            sortable = sort(items)

            self.assertEqual(sortable, reference_array)

    def test_count_sort(self):
        reference_numbers = list(random_numbers())
        reference_numbers.sort()

        numbers = list(random_numbers())
        numbers = count_sort(numbers, key=lambda x: x)

        self.assertEqual(reference_numbers, numbers)

if __name__ == "__main__":
    unittest.main()
