import unittest
from trie import Trie


def words():
    return (
        "foo",
        "foobar",
        "bar",
        "sus",
        "sussy",
        "abcabc",
        "abc",
        "a",
    )


class TrieTest(unittest.TestCase):



    def test_simple_trie(self):
        t = Trie()

        word = "foo"

        self.assertTrue(word not in t)
        t.insert(word)

        self.assertTrue(t.has_prefix(word[:-1]))
        self.assertTrue(word in t)


    def test_trie_insert(self, words=words()):
        t = Trie()

        for w in words:
            t.insert(w)
            self.assertTrue(w in t)


    def test_trie_queries(self, words=words()):
        t = Trie()

        for w in words:
            t.insert(w)

        for w in words:
            for i in range(1, len(w)):
                self.assertTrue(t.has_prefix(w[:i]))

if __name__ == "__main__":
    unittest.main()
