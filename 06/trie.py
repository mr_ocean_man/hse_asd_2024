from dataclasses import dataclass, field
from typing import Mapping


@dataclass
class Node:
    children: Mapping[str, "Node"] = field(default_factory=dict)
    is_terminal: bool = False

    def child(self, letter: str) -> "Node":
        if letter not in self.children:
            self.children[letter] = Node()

        return self.children[letter]


class Trie:
    __root: Node

    def __init__(self) -> None:
        self.__root = Node()

    def insert(self, key: str):
        node = self.__root
        sz = len(key)

        for i, letter in enumerate(key, start=1):
            node = node.child(letter)
            node.is_terminal = i == sz

    def __search(self, key: str, prefix: bool):
        if not key:
            return False

        node = self.__root

        for letter in key:
            if letter not in node.children:
                return False

            node = node.children[letter]

        return prefix or node.is_terminal

    def __contains__(self, key: str):
        return self.search(key)

    def search(self, key: str):
        return self.__search(key, prefix=False)

    def has_prefix(self, key: str):
        return self.__search(key, prefix=True)

    def __repr__(self) -> str:
        return self.__root.children.__repr__()
