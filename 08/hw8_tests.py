"""
module with tests for floyd algorithm
"""

import unittest
from floyd import floyd, INF
from typing import List


def input_from_file(input_path) -> List[tuple]:
    """

    :param: input file path
    :return: matrix with adjacency matrix
    """
    input_values = []
    with open(input_path, 'r') as file:
        input_values = []
        for line in file.readlines():
            line = line.strip('\n').split(',')
            input_values.append([INF if num == "INF" else int(num) for num in line])

        file.close()

    return input_values


def write_to_file(dist_matr, output_path):
    """

    :param dist_matr: distance matrix
    :param output_path: output file path
    :return:
    """
    with open(output_path, 'w', encoding='utf-8') as file:
        for row in dist_matr:
            file.write(' '.join(["INF" if num == INF else str(num) for num in row]) + '\n')

        file.close()


class TestFloydAlgo(unittest.TestCase):
    """
    Class with test cases
    """


    def test_input(self):
        input_matrix = input_from_file("/Users/anatoliy/PycharmProjects/hse_asd_spring_2024/08/test_input.txt")
        real_matrix = [[0, 5, INF, 10],
                       [INF, 0, 3, INF],
                       [INF, INF, 0, 1],
                       [INF, INF, INF, 0]]

        self.assertEqual(input_matrix, real_matrix)
    

    def test_floyd_algo(self):
        adj_matrix = input_from_file("/Users/anatoliy/PycharmProjects/hse_asd_spring_2024/08/test_input.txt")
        distance_matrix = floyd(adj_matrix)
        real_matrix = [[0, 5, 8, 9],
                       [INF, 0, 3, 4],
                       [INF, INF, 0, 1],
                       [INF, INF, INF, 0]]
        self.assertEqual(distance_matrix, real_matrix)

if __name__ == "__main__":
    unittest.main()
        