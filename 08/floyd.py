"""
floyd algorithm module
"""

from typing import List

INF = 1e10


def floyd(adj_matr: List[List[int]]) -> List[List[int]]:
    """
    Floyd algorithm realization

    Args:
        adj_matr (List[List[int]]): adjacency matrix

    Returns:
        List[List[int]]: matrix with smallest path distance between vertex or
        INF if path not implemented
    """
    dist_matr = adj_matr
    for intermidiate in range(len(adj_matr)):
        for source in range(len(adj_matr)):
            for destination in range(len(adj_matr)):
                dist_matr[source][destination] = min(
                    dist_matr[source][destination],
                    dist_matr[source][intermidiate]
                    + dist_matr[intermidiate][destination])

    # print_solution(dist_matr)
    return dist_matr


def print_solution(dist: List[List[int]]):
    """
    print distance matrix

    Args:
        dist (List[List[int]]): distance matrix
    """
    for row in dist:
        for val in row:
            if val == INF:
                print("INF", end=" ")
            else:
                print(val, end=" ")


